#!/bin/sh

# Cache
# php artisan route:cache --quiet
# php artisan config:cache --quiet

# Permissions
# On Alpine Apache's user and groups are apache
chown -R apache:apache /var/www

# Migrations
# Launch the httpd in foreground
rm -rf /run/apache2/* || true && /usr/sbin/httpd -DFOREGROUND